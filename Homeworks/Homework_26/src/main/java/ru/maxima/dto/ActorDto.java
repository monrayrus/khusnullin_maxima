package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.models.Actor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ActorDto {
    private long id;
    private String firstName;
    private String lastName;

    public static ActorDto from(Actor actor) {
        return  ActorDto.builder()
                .id(actor.getId())
                .firstName(actor.getFirstName())
                .lastName(actor.getLastName())
                .build();
    }

    public static List<ActorDto> from(List<Actor> actors) {
        return actors.stream().map(ActorDto::from).collect(Collectors.toList());
    }
}
