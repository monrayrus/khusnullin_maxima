package ru.maxima.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class ApplicationInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // прописать запуск DispatcherServlet-а
        // мы говорим, что у нас будет конфигурация веба на аннотациях
        AnnotationConfigWebApplicationContext springWebContext = new AnnotationConfigWebApplicationContext();
        // регистрируем в веб-конфигурации нашу конфигурацию
        springWebContext.register(ApplicationConfig.class);
        // нужно прописать, чтобы при старте приложения подключались наши конфигурации (ApplicationConfig)
        ContextLoaderListener springContextLoaderListener = new ContextLoaderListener(springWebContext);
        servletContext.addListener(springContextLoaderListener);
        // зарегистрировать DispatcherServlet
        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher",
                new DispatcherServlet(springWebContext));
        dispatcherServlet.setLoadOnStartup(1);
        dispatcherServlet.addMapping("/");
    }
}
