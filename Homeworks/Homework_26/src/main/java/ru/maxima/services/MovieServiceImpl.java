package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.MovieDto;
import ru.maxima.models.Actor;
import ru.maxima.models.Movie;
import ru.maxima.repositories.ActorsRepository;
import ru.maxima.repositories.MoviesRepository;

import java.util.List;

import static ru.maxima.dto.MovieDto.from;
import static ru.maxima.dto.ActorDto.from;
@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final ActorsRepository actorsRepository;
    private final MoviesRepository moviesRepository;

    @Override
    public void addMovie(MovieDto movie) {
        Movie newMovie = Movie.builder()
                .id(movie.getId())
                .genre(movie.getGenre())
                .title(movie.getTitle())
                .year(movie.getYear())
                .build();
        moviesRepository.save(newMovie);
    }

    @Override
    public List<MovieDto> getAllMovies() {
        return from(moviesRepository.findAll());
    }

    @Override
    public void addActorToMovie(Long movieId, ActorDto actorForm) {
        Movie movie = moviesRepository.getById(movieId);
        Actor actor = actorsRepository.getById(actorForm.getId());
        actor.setMovie(movie);
        actorsRepository.save(actor);
    }

    @Override
    public List<ActorDto> getActorsByMovie(Long movieId) {
        return from(actorsRepository.findAllByMovieId(movieId));
    }

    @Override
    public List<ActorDto> getActorsWithoutMovie() {
        return from(actorsRepository.findAllByMovieIsNull());
    }
}
