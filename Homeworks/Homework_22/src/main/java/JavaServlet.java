import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class JavaServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.println("<h1>Google</h1>\n" +
                "<p>Input your search request here </p>\n" +
                "<br>\n" +
                "<form action=\"http://google.com/search\">\n" +
                "\t<label for=\"search\">Search request:</label>\n" +
                "\t<input id=\"search\" type=\"text\" name=\"q\" placeholder=\"Input your request\">\n" +
                "\t<input type=\"submit\" name=\"\" value=\"FIND IT\">\n" +
                "</form>");
    }
}
