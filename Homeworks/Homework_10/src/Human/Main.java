package Human;

public class Main {

    public static void main(String[] args) {
        Human humanOne = new Human(38, "Mark", "Gordon", true);
        Human humanTwo = new Human(11, "John", "McKinsey", false);
        Human humanThree = new Human(19, "Frank", "Sinatra", false);
        Human humanFour = new Human(15, "Andre", "Shepard", true);
        Human humanFive = new Human(19, "Frank", "Sinatra", false);

        Human[] humans = {humanOne, humanTwo, humanThree, humanFour, humanFive};

        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i].toString());
        }

        System.out.println(humanFive.equals(humanFour));
        System.out.println(humanFive.equals(humanThree));
    }
}
