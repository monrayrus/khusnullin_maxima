package Human;

public class Human {
    int age;
    String firstName;
    String lastName;
    boolean isAdult;
    boolean isWorker;

    public Human(int age, String firstName, String lastName, boolean isWorker) {
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
        if (age > 17) {
            isAdult = true;
        }
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Human)) {
            return false;
        }

        if (this == object) {
            return true;
        }
        Human that = (Human) object;
        return this.age == that.age && this.firstName == that.firstName && this.lastName == that.lastName && that.isAdult == this.isAdult && this.isWorker == that.isWorker;
    }

    @Override
    public String toString() {
        if (isWorker && isAdult) {
            return "[Working adult human]: name " + firstName + ", lastname " + lastName + ", age " + age;
        }
        if (isWorker) {
            return "[Working minor human]: name " + firstName + ", lastname " + lastName + ", age " + age;
        }
        if (isAdult) {
            return "[Not working adult human]: name " + firstName + ", lastname " + lastName + ", age " + age;
        }
        return "[Not working minor human]: name " + firstName + ", lastname " + lastName + ", age " + age;
    }
}
