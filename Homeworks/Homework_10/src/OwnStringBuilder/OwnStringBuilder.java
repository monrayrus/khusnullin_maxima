package OwnStringBuilder;

public class OwnStringBuilder {
    private char[] chars;
    private String string;

    public OwnStringBuilder(String string) {
        this.string = string;
    }

    public void append(String input) {
        chars = new char[string.length() + input.length()];
        char[] stringChar = string.toCharArray();
        char[] inputChar = input.toCharArray();
        int count = 0;
        for (int i = 0; i < stringChar.length; i++) {
            chars[i] = stringChar[i];
            count++;
        }
        for (int j = 0; j < inputChar.length; j++) {
            chars[count++] = inputChar[j];
        }
    }

    @Override
    public String toString() {
        return new String(chars);
    }
}
