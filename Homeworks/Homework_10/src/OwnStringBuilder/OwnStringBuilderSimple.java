package OwnStringBuilder;

public class OwnStringBuilderSimple {
    private String string;

    public OwnStringBuilderSimple(String input) {
        this.string = input;
    }

    public void appendSimpleVersion(String input) {
        string += input;
    }

    @Override
    public String toString() {
        return string;
    }
}
