package OwnStringBuilder;

public class Main {
    public static void main(String[] args) {
        // решил проверить заодно разницу в скорости исполнения
        // перый метод на основе конкатенации строк
        long start1 = System.nanoTime();

        OwnStringBuilderSimple builderSimple = new OwnStringBuilderSimple("INPUT TEXT ");
        builderSimple.appendSimpleVersion("added text");
        System.out.println(builderSimple.toString());

        long finish1 = System.nanoTime();
        long elapsed1 = finish1 - start1;
        System.out.println("Прошло времени, builderSimple: " + elapsed1);

        // второй метод на основе массивов char
        long start = System.nanoTime();

        OwnStringBuilder builder = new OwnStringBuilder("INPUT TEXT ");
        builder.append("added text");
        System.out.println(builder.toString());

        long finish = System.nanoTime();
        long elapsed = finish - start;
        System.out.println("Прошло времени, builder: " + elapsed);
    }
}
