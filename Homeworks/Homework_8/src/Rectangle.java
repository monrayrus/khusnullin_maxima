public class Rectangle extends GeometricShape {

    private double a;
    private double b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
        squareOfFigure = a * b;
        perimeterOfFigure = (2 * a) + (2 * b);
    }

    public Rectangle() {

    }

    @Override
    public void scale(double x) {
        squareOfFigure = a * x * b * x;
        perimeterOfFigure = (2 * a * x) + (2 * b * x);
    }

    @Override
    public void getCoordinate() {
        System.out.println("Rectangle coordinate - X: " + x + ", Y: " + y);
    }

    @Override
    public void result() {
        System.out.println("Rectangle square: " + resultOfSquare() + ", perimeter: " + resultOfPerimeter());
    }
}