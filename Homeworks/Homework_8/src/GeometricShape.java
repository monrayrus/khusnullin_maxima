public abstract class GeometricShape implements Relocatable, Scalability {
    double squareOfFigure;
    double perimeterOfFigure;
    // координаты центра фигуры по умолчанию
    protected int x = 0;
   	protected int y = 0;

    public double resultOfSquare() {
        return squareOfFigure;
    }

    public double resultOfPerimeter() {
        return perimeterOfFigure;
    }

    public abstract void result();


    @Override
    public void relocate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void getCoordinate() {
        System.out.println("X: " + x + ", Y: " + y);
    }

}