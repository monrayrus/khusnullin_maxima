public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        Ellipse ellipse = new Ellipse(5, 2);
        Rectangle rectangle = new Rectangle(4, 2);
        Square square = new Square(5);

        circle.result();
        circle.getCoordinate();
        circle.relocate(1, 5);
        circle.scale(1.2);
        circle.getCoordinate();
        circle.result();

		ellipse.result();
		ellipse.getCoordinate();
		ellipse.relocate(2, 4);
		ellipse.scale(1.1);
		ellipse.getCoordinate();
		ellipse.result();

		rectangle.result();
		rectangle.getCoordinate();
		rectangle.relocate(-1, -6);
		rectangle.scale(2);
		rectangle.getCoordinate();
		rectangle.result();

		square.result();
		square.getCoordinate();
		square.relocate(3, 1);
		square.scale(1.5);
		square.getCoordinate();
		square.result();
    }
}

