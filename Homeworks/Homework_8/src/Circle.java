public class Circle extends Ellipse {

    private double radius;

    public Circle(int radius) {
        super();
        this.radius = radius;
        squareOfFigure = radius * Math.PI;
        perimeterOfFigure = 2 * Math.PI * radius;
    }


    @Override
    public void scale(double x) {
        squareOfFigure = radius * x * Math.PI;
        perimeterOfFigure = 2 * Math.PI * radius * x;
    }

    @Override
    public void result() {
        System.out.println("Circle square: " + resultOfSquare() + ", perimeter: " + resultOfPerimeter());
    }

    @Override
    public void getCoordinate() {
        System.out.println("Circle coordinate - X: " + x + ", Y: " + y);
    }
}