public class Ellipse extends GeometricShape {

    private double a;
    private double b;

    public Ellipse(int a, int b) {  // a - большая полуось, b - малая полуось
        this.a = a;
        this.b = b;
        if (a >= b) {
            squareOfFigure = Math.PI * a * b;
            perimeterOfFigure = 4 * (Math.PI * a * b + (a - b)) / a + b;
        }
    }

    public Ellipse() {

    }

    @Override
    public void scale(double x) {
        a *= x;
        b *= x;
        squareOfFigure = Math.PI * a * b;
        perimeterOfFigure = 4 * (Math.PI * a * b + (a - b)) / a + b;
    }

    @Override
    public void getCoordinate() {
        System.out.println("Ellipse coordinate - X: " + x + ", Y: " + y);
    }

    @Override
    public void result() {
        System.out.println("Ellipse square: " + resultOfSquare() + ", perimeter: " + resultOfPerimeter());
    }
}