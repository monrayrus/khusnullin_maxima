public interface Scalability {
    void scale(double x);
}