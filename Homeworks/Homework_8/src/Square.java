public class Square extends Rectangle {

    private double a;

    public Square(int a) {
        super();
        this.a = a;
        squareOfFigure = a * a;
        perimeterOfFigure = 4 * a;
    }

    @Override
    public void scale(double x) {
        a *= x;
        squareOfFigure = a * a;
        perimeterOfFigure = 4 * a;
    }

    @Override
    public void getCoordinate() {
        System.out.println("Square coordinate - X: " + x + ", Y: " + y);
    }

    @Override
    public void result() {
        System.out.println("Square square: " + resultOfSquare() + ", perimeter: " + resultOfPerimeter());
    }
}