public interface Relocatable {
    void relocate(int x, int y);
}