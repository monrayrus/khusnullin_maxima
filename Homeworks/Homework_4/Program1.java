class Program1 {
	public static void main (String[] args) {
		int[] array = {1, 5, 0, 6, 9}; //пример массива
		System.out.print(parse(array));
	}

	public static int parse (int[] array) {
		int result = 0;
		for(int i = 0; i < array.length ; i++) {
			result = result * 10 + array[i];
		}
		return result;
	}
}
