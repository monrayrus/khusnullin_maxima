import java.util.Scanner;

class Program2 {
	public static void main (String[] args) {
		System.out.println(resultOfTask(inputArray()));
	}

	public static int[] inputArray() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Введите размер массива");
		int numberOfDigits = scanner.nextInt();
		int[] arrayOfDigits = new int[numberOfDigits];
		System.out.println("Введите числа");
		for(int i = 0; i < numberOfDigits; i++) {
			arrayOfDigits[i] = scanner.nextInt();
		}
		return arrayOfDigits;
	}

	public static int resultOfTask (int[] array) {
		int number = 0;
		int count = 0;
		int max = 0;
		for (int i = 0; i < array.length; i++) {
			for(int j = 1; j < array.length;j++) {
				if(array[i] == array[j]) {
					count++;
				}
			}
			if(count > max) {
				number = array[i];
				max = count;
				count = 0;
			} else {
				count = 0;
			}
		}
		return number;
	}
}