import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<Car> carList;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            carList = reader.lines().map(Main::createCar).collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. - " + carList.stream()
                .distinct().filter((p) -> p.getPrice() >= 700000&& p.getPrice() <= 800000)
                .count());

        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег:");
        carList.stream()
                .filter((car) -> car.getColor().equals("black")|| car.getMileage() == 0)
                .map(car -> car.getNumber())
                .forEach(System.out::println);

        System.out.println("Цвет автомобиля с минимальной стоимостью - " + carList.stream()
                .min(Car::compareTo)
                .map(car -> car.getColor().toUpperCase())
                .get());

        double camryAverage = carList.stream()
                        .filter(car -> car.getModel().equals("camry"))
                        .mapToInt(Car::getPrice).average().getAsDouble();
        System.out.println("Средняя стоимость Camry - " + (int)camryAverage);
    }



    private static Car createCar(String string) {
        try {
            String[] array = string.split(" ");
            return new Car(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4]));
        } catch (Exception e) {
            System.err.println("Error");
        }
        return null;
    }
}
