import java.util.Objects;

public class Car implements Comparable<Car>{
    private String number;
    private String model;
    private String color;
    private int mileage;
    private int price;

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    public Car(String number, String model, String color, int mileage, int cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = cost;
    }

    @Override
    public String toString() {
        return "Car " +
                "[number=" + number +
                "], [model='" + model.toUpperCase() + '\'' +
                "], [color='" + color.toUpperCase() + '\'' +
                "], [mileage=" + mileage +
                "], [price=" + price +
                "]";
    }

    @Override
    public int compareTo(Car o) {
            return this.getPrice() - o.getPrice();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model) && Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, color);
    }
}
