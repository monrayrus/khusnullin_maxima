package solution;

import solution.framework.DocumentsFramework;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args)  {
        DocumentsFramework framework = new DocumentsFramework();
        Certificate certificate = framework.generate(Certificate.class, LocalDate.of(2022, 1, 1), "Сертификат" , 1);
        System.out.println(certificate);
    }
}
