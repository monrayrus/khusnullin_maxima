package solution;

import solution.framework.CheckRange;
import solution.framework.DefaultValue;
import solution.framework.Document;

import java.time.LocalDate;
import java.util.StringJoiner;


public class Certificate implements Document {
    private LocalDate date;
    private String name;
    @CheckRange(min = 0, max = 10)
    private int activeYears;

    @DefaultValue(value = "ФСС")
    private String from;

    public Certificate(LocalDate date, String name, Integer activeYears) {
        this.date = date;
        this.name = name;
        this.activeYears = activeYears;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Certificate.class.getSimpleName() + "[", "]")
                .add("date=" + date)
                .add("name='" + name + "'")
                .add("active years='" + activeYears + "'")
                .add("from='" + from + "'")
                .toString();
    }
}
