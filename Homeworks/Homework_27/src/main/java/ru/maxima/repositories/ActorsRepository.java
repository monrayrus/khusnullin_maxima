package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Actor;

import java.util.List;

public interface ActorsRepository extends JpaRepository<Actor, Long> {

    List<Actor> findAllByMovieId(long movieId);
    List<Actor> findAllByMovieIsNull();

}
