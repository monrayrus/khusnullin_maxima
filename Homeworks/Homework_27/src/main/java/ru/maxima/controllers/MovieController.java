package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.MovieDto;
import ru.maxima.services.MovieService;

@Controller
public class MovieController {
    @Autowired
    private MovieService movieService;

    @RequestMapping("/films")
    public String getMovies(Model model) {
        model.addAttribute("films", movieService.getAllMovies());
        return "films";
    }

    @RequestMapping("/films/{film-id}/actors")
    public String getActorsOfMoviePage(@PathVariable("film-id") Long filmId, Model model) {
        model.addAttribute("actors", movieService.getActorsByMovie(filmId));
        model.addAttribute("actors_free", movieService.getActorsWithoutMovie());
        return "actors_in_film";
    }

    @RequestMapping(value = "/films/{film-id}/actors", method = RequestMethod.POST)
    public String addActorToMovie(@PathVariable("film-id") Long filmId, ActorDto actor) {
        movieService.addActorToMovie(filmId, actor);
        return "redirect:/films/" + filmId + "/actors";
    }

    @RequestMapping(value = "/films", method = RequestMethod.POST)
    public String addMovie(MovieDto form) {
        movieService.addMovie(form);
        return "redirect:/films";
    }
}
