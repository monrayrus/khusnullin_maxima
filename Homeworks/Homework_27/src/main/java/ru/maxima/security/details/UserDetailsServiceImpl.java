package ru.maxima.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

@RequiredArgsConstructor
@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountsRepository accountsRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = accountsRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Not found"));
        return new UserDetailsImpl(account);
    }
}
