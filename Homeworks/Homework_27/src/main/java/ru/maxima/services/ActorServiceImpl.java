package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.models.Actor;
import ru.maxima.repositories.ActorsRepository;

import java.util.List;

import static ru.maxima.dto.ActorDto.from;

@Service
@RequiredArgsConstructor
public class ActorServiceImpl implements ActorService {

    private final ActorsRepository actorsRepository;
    @Override
    public void addActor(ActorDto actor) {
        Actor newActor = Actor.builder()
                .id(actor.getId())
                .firstName(actor.getFirstName())
                .lastName(actor.getLastName())
                .build();
    actorsRepository.save(newActor);
    }

    @Override
    public List<ActorDto> getAllActors()
    {
        return from(actorsRepository.findAll());
    }
}
