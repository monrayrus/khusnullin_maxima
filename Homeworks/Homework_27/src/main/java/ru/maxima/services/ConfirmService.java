package ru.maxima.services;

public interface ConfirmService {
    boolean confirm(String uuid);
}
