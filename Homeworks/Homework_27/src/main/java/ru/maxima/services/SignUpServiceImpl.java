package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.maxima.dto.SignUpForm;
import ru.maxima.mail.MailUtil;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;

    private final AccountsRepository accountsRepository;

    private final MailUtil mailUtil;

    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.NOT_CONFIRMED)
                .confirmUUID(UUID.randomUUID().toString())
                .build();
        mailUtil.sendMailToConfirm(account.getFirstName(), account.getEmail(), account.getConfirmUUID());
        accountsRepository.save(account);
    }
}
