package ru.maxima.services;

import ru.maxima.dto.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
