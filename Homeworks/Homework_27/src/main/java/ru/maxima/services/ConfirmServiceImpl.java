package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConfirmServiceImpl implements ConfirmService {

    private final AccountsRepository accountsRepository;

    @Override
    public boolean confirm(String uuid) {
        Optional<Account> account = accountsRepository.findByConfirmUUID(uuid);
        if (account.isPresent()) {
            Account forSave = account.get();
            forSave.setState(Account.State.CONFIRMED);
            accountsRepository.save(forSave);
            return true;
        }
        return false;
    }
}
