

public class Main {

    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(10);
        list.add(15);
        list.add(20);
        list.add(25); // первое появление числа 25
        list.add(30);
        list.add(35); // этот элемент будет удален
        list.add(40);
        list.add(25); // второе появление числа 25
        list.add(55);
        list.add(60);
        list.add(70); // эти элемены будут удалены
        list.add(70); // эти
        list.add(75);
        list.addToBegin(80); // сдвигает все числа и получает индекс 0
        list.addToBegin(85); // сдвигает все числа и получает индекс 0, а 80 получает индекс 1


        System.out.println(list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        System.out.println(+list.indexOf(25));
        System.out.println(list.lastIndexOf(25));

        list.remove(6);
        list.removeAll(70);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }
}
