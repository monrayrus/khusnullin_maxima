package ru.maxima.services;

import ru.maxima.dto.ActorDto;

import java.util.List;

public interface ActorService {
    void addActor(ActorDto actor);
    List<ActorDto> getAllActors();

}
