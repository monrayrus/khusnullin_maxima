package ru.maxima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework26SpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework26SpringBootApplication.class, args);
    }

}
