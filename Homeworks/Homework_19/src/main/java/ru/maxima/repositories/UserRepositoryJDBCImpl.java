package ru.maxima.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.maxima.models.User;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class UserRepositoryJDBCImpl implements UsersRepository {

    private final DataSource dataSource;

    private final JdbcTemplate jdbcTemplate;

    public UserRepositoryJDBCImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL =
            "select id, email, password from users where email = ?";
    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from users order by id";
    //language=SQL
    private static final String SQL_UPDATE_PASSWORD =
            "update users set password = ? where id = ?;";
    //language=SQL
    private static final String SQL_INSERT =
            "insert into users(email, password) values (?, ?)";

    @Override
    public void save(User user) {
        jdbcTemplate.update(SQL_INSERT,
                user.getEmail(),
                user.getPassword());
    }

    private final RowMapper<User> rowToAccountMapper = (row, rowNumber) -> new User(
            row.getInt("id"),
            row.getString("email"),
            row.getString("password")
    );


    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL, rowToAccountMapper, email));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, rowToAccountMapper);
    }

    @Override
    public void update(User user) {
        String password = new Scanner(System.in).nextLine();
        jdbcTemplate.update(SQL_UPDATE_PASSWORD,
                password,
                user.getId());
    }
}
