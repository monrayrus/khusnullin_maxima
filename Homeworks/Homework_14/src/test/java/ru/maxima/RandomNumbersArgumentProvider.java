package ru.maxima;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class RandomNumbersArgumentProvider implements ArgumentsProvider {
    private Random random = new Random();

    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        List<Arguments> numbers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String string = "";
            boolean isInt = random.nextBoolean();
            if(isInt) {
                int number = random.nextInt();
                string = Integer.toString(number);
            } else {
                double number = -100 + (100 - (-100)) * random.nextDouble();
                string = Double.toString(number);
            }

            Arguments argument = Arguments.of(string);
            numbers.add(argument);
        }
        return numbers.stream();
    }
}
