import java.util.Scanner;

class Program {
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		while(number != -1) {
			if(isEven(number)) {
				System.out.println(minDigit(number));
			}
			if(!isEven(number)) {
				System.out.println(getDigitsAverage(number));
			}
		number = scanner.nextInt();
		}
	}

	public static int minDigit(int number) {
		int min = 9;
		while(number > 0) {
			int temp = number % 10;
			if(temp < min) {
				min = temp;
			}
			number /= 10;
		}
		return min;
	}

	public static boolean isEven(int number) {
		return number % 2 == 0;
	}

	public static double getDigitsAverage(int number) {
		double count = 0;
		int result = 0;
		while(number != 0) {
			result += number % 10;
			number = number / 10;
			count++;
		}
		return result/count;
	}
}