package com.example.demo.models;

public enum Role {
	CUSTOMER, MANAGER, ADMIN;
}
