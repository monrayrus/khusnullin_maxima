package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.maxima.services.ProductService;

@Controller
public class allProductsController{

    @Autowired
    private ProductService productService;

    @RequestMapping("/products")
    public String getUsers(Model model) {
        model.addAttribute("products", productService.getAllProducts());
        return "allProductsList";
    }
}
