package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ProductDto;
import ru.maxima.models.Product;
import ru.maxima.repositories.ProductRepository;

import java.util.List;

import static ru.maxima.dto.ProductDto.from;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl (ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return from(productRepository.findAll());
    }

}
