package ru.maxima.services;

import ru.maxima.dto.ProductDto;
import ru.maxima.models.Product;

import java.util.List;

public interface ProductService {
    List<ProductDto> getAllProducts();
}
