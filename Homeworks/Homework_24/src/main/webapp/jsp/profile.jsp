<%--
  Created by IntelliJ IDEA.
  User: Monray
  Date: 06.10.2021
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1 style="color: ${color};">Profile Page</h1>
<h2 style="color: ${color};">Имя пользователя: ${account.first_name} ${account.last_name}</h2>
<form>
    <input type="radio" id="redColor" name="color" value="red" /> <label for="redColor">Red</label>
    <input type="radio" id="blueColor" name="color" value="blue" /> <label for="blueColor">Blue</label>
    <input type="radio" id="greenColor" name="color" value="green" /> <label for="greenColor">Green</label>
    <input type="radio" id="cyanColor" name="color" value="cyan" /> <label for="cyanColor">Cyan</label>
    <input type="radio" id="yellowColor" name="color" value="yellow" /> <label for="yellowColor">Yellow</label>
    <input type="submit" value="Submit" />
</form>
<h2><a href="/addProduct">Добавить продукт</a></h2>
<h2><a href="/allProductsList">Список всех продуктов</a></h2>
<h2><a href="/search">Поиск продукта</a></h2>
<h2><a href="/logout">Выход</a></h2>
</body>
</html>

