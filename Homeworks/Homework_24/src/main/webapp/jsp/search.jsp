<%--
  Created by IntelliJ IDEA.
  User: Monray
  Date: 29.09.2021
  Time: 14:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<script>
    function search(name) {
        let request = new XMLHttpRequest();

        request.open('GET', '/search?name=' + name, false);

        request.send();

        if (request.status !== 200) {
            alert("Ошибка!")
        } else {
            let html = '<tr>' +
                '<th>Id</th>' +
                '<th>Name</th>' +
                '<th>Type</th>' +
                '</tr>';


            let response = JSON.parse(request.response);

            for (let i = 0; i < response['products'].length; i++) {
                html += '<tr>';
                html += '<td>' + response['products'][i]['id'] + '</td>';
                html += '<td>' + response['products'][i]['name'] + '</td>';
                html += '<td>' + response['products'][i]['type'] + '</td>';
                html += '</tr>'
            }

            document.getElementById('product_table').innerHTML = html;
        }
    }
</script>
<label for="name">Enter name for search</label>
<input id="name" name="name" placeholder="Name" onkeyup="search(document.getElementById('name').value)">
<br>
<table id="product_table">

</table>

<h2><a href="/addProduct">Добавить продукт</a></h2>
<h2><a href="/allProductsList">Список всех продуктов</a></h2>
<h2><a href="/profile">Профиль</a></h2>
<h2><a href="/logout">Выход</a></h2>
</body>
</html>
