<%--
  Created by IntelliJ IDEA.
  User: Monray
  Date: 28.09.2021
  Time: 20:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.List" %>
<%@ page import="ru.maxima.servlets.ProductsServlet" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<script>
    function addProduct(name, type) {
        let body = {
            "name": name,
            "type": type
        };

        let request = new XMLHttpRequest();

        request.open('POST', '/addProduct', false);
        request.setRequestHeader("Content-Type", "application/json");
        request.send(JSON.stringify(body));

        if (request.status !== 201) {
            alert("Ошибка!")
        }

    }
</script>
<body>
<div>
    <label for="name">Введите название продукта:</label>
    <input id="name" name="name" placeholder="Example: 365 days"/>
    <label for="type">Введите тип продукта:</label>
    <input id="type" name="type" placeholder="Example: milk">
    <button onclick="addProduct(
        document.getElementById('name').value,
        document.getElementById('type').value)">Add
    </button>
</div>

<h2><a href="/profile">Профиль</a></h2>
<h2><a href="/allProductsList">Список всех продуктов</a></h2>
<h2><a href="/search">Поиск продукта</a></h2>
<h2><a href="/logout">Выход</a></h2>
</body>
</html>
