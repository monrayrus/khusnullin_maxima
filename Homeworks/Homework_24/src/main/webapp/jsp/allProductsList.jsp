<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Monray
  Date: 29.09.2021
  Time: 13:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.maxima.dto.ProductDto" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1 style="${color}">List of products - ${products.size()}
</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Название</th>
        <th>Тип</th>
    </tr>
    <c:forEach items="${products}" var="product">
        <tr>
            <td>${product.id}</td>
            <td>${product.name}</td>
            <td>${product.type}</td>
        </tr>
    </c:forEach>
</table>

<h2><a href="/addProduct">Добавить продукт</a></h2>
<h2><a href="/profile">Профиль</a></h2>
<h2><a href="/search">Поиск продукта</a></h2>
<h2><a href="/logout">Выход</a></h2>
</body>
</html>
