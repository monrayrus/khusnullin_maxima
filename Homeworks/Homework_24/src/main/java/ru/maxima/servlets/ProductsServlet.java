package ru.maxima.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.maxima.dto.ProductDto;
import ru.maxima.models.Product;
import ru.maxima.services.ProductService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addProduct")
public class ProductsServlet extends HttpServlet {
    private ObjectMapper objectMapper;
    private ProductService productService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext)servletContext.getAttribute("springContext");
        this.objectMapper = applicationContext.getBean(ObjectMapper.class);
        this.productService = applicationContext.getBean(ProductService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("jsp/addProduct.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String productAsJson = request.getReader().readLine();
        ProductDto product = objectMapper.readValue(productAsJson, ProductDto.class);
        System.out.println(product);
        productService.addProduct(product);
        response.setStatus(201);

    }
}
