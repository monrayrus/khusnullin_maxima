package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import ru.maxima.exceptions.IncorrectPassOrEmailException;
import ru.maxima.services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/signIn")

public class SignInServlet extends HttpServlet {

    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        try {
            userService.signIn(email, password);
            HttpSession session = req.getSession(true);
            session.setAttribute("isAuth", true);
            resp.sendRedirect("/profile");
        } catch (IncorrectPassOrEmailException e) {
            resp.sendRedirect("/signIn");
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.userService = applicationContext.getBean(UserService.class);
    }
}
