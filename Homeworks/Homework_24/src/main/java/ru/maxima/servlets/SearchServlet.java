package ru.maxima.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.maxima.dto.ProductDto;
import ru.maxima.dto.ProductResponseDto;
import ru.maxima.services.ProductService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/search")
public class SearchServlet extends HttpServlet {

    private ProductService productService;
    private ObjectMapper objectMapper;

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.productService = applicationContext.getBean(ProductService.class);
        this.objectMapper = applicationContext.getBean(ObjectMapper.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("name") == null) {
            request.getRequestDispatcher("/jsp/search.jsp").forward(request, response);
        } else {
            List<ProductDto> products = productService.search(request.getParameter("name"));
            String jsonResponse = objectMapper.writeValueAsString(new ProductResponseDto(products));
            response.setStatus(200);
            response.setContentType("application/json");
            response.getWriter().println(jsonResponse);
        }
    }
}
