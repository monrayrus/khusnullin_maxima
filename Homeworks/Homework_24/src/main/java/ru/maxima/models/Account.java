package ru.maxima.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Account {
    private String first_name;
    private String last_name;
    private String email;
    private String hashPassword;

    public Account(String email, String hashPassword, Long id) {
        this.email = email;
        this.hashPassword = hashPassword;
        this.id = id;
    }

    private Long id;

}
