package ru.maxima.services;

public interface UserService {
    void signUp(String email, String password);
    void signIn(String email, String password);
}
