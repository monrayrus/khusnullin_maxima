package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.maxima.exceptions.IncorrectPassOrEmailException;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountRepository;

@Service
public class UserServiceImpl implements UserService {
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public void signUp(String email, String password) {
        Account account = Account.builder()
                .email(email)
                .hashPassword(passwordEncoder.encode(password))
                .build();

        accountRepository.save(account);
    }

    @Override
    public void signIn(String email, String password) {
        Account account = accountRepository.findByEmail(email).orElseThrow(IncorrectPassOrEmailException::new);

        if(!passwordEncoder.matches(password, account.getHashPassword())) {
            throw new IncorrectPassOrEmailException();
        }
    }
}
