package ru.maxima.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebFilter("/*")
public class AuthFilter implements Filter {

    private static final String IS_AUTH_ATTRIBUTE_NAME = "isAuth";
    private static final List<String> PROTECTED_URLS = Arrays.asList(
            "/search",
            "/addProduct",
            "/allProductsList",
            "/profile"
    );

    private static final List<String> NOT_ACCESSED_URLS = Arrays.asList(
            "/signIn",
            "/signUp"
    );

    private static final String DEFAULT_REDIRECT_URL = "/profile";
    private static final String DEFAULT_SIGNIN_URL = "/signIn";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if(isProtected(request.getRequestURI())) {
            if(isAuth(request)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            } else {
                response.sendRedirect(DEFAULT_SIGNIN_URL);
                return;
            }
        }

        if(isAuth(request)&&notAccessed(request.getRequestURI())) {
            response.sendRedirect(DEFAULT_REDIRECT_URL);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }

    private boolean isProtected(String url) {
        return PROTECTED_URLS.contains(url);
    }

    private boolean notAccessed(String url) {
        return NOT_ACCESSED_URLS.contains(url);
    }

    private boolean isAuth(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return false;
        }
        Boolean result = (Boolean)session.getAttribute(IS_AUTH_ATTRIBUTE_NAME);
        return result != null && result;
    }
}
