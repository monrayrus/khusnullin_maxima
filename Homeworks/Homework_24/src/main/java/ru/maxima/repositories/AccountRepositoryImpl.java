package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.maxima.models.Account;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Optional;
@Repository
public class AccountRepositoryImpl implements AccountRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    //language=SQL
    private static final String SQL_INSERT =
            "insert into account(email, hash_password) values (:email, :password) RETURNING id";

    //language=SQL
    private static final String SQL_FIND_BY_EMAIL =
            "select * from account where email = :email";

    private static final RowMapper<Account> accountRowMapper = (row, rowNumber) -> Account.builder()
            .id(row.getLong("id"))
            .email(row.getString("email"))
            .hashPassword(row.getString("hash_password"))
            .build();
    @Autowired
    public AccountRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Account account) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, (new MapSqlParameterSource()
                .addValue("email", account.getEmail())
                .addValue("password", account.getHashPassword())), keyHolder, new String[]{"id"});
                account.setId(keyHolder.getKey().longValue());
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_FIND_BY_EMAIL,
                    Collections.singletonMap("email", email),
                    accountRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
