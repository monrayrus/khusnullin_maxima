package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.maxima.models.Product;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
@Repository
public class ProductRepositoryImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into products(name, type) values (:name, :type) RETURNING id";

    //language=SQL
    private static final String SQL_GET_ALL_PRODUCTS = "select * from products order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_NAME_LIKE = "select * from products where name ilike :name";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProductRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .type(row.getString("type"))
            .build();

    @Override
    public void save(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, (new MapSqlParameterSource()
                .addValue("name", product.getName())
                .addValue("type", product.getType())), keyHolder, new String[]{"id"});
        product.setId(keyHolder.getKey().longValue());
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_GET_ALL_PRODUCTS, productRowMapper);
    }

    @Override
    public List<Product> searchProduct(String name) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_NAME_LIKE,
                Collections.singletonMap("name", "%" + name + "%"),
                productRowMapper);
    }
}
