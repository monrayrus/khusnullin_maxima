package ru.maxima.repositories;

import ru.maxima.models.Product;

import java.util.List;

public interface ProductRepository {
    void save(Product product);
    List<Product> findAll();
    List<Product> searchProduct(String name);
}
