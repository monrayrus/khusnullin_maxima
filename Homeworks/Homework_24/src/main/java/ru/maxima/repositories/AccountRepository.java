package ru.maxima.repositories;

import ru.maxima.models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {
    void save(Account account);
    Optional<Account> findByEmail(String email);
}
