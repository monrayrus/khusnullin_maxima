import java.util.Scanner;

class Program {
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		int count = 0;
		for(int i = 10; i > 0; i--) {
			int input = scanner.nextInt();
			int result = 0;
			while (input != 0) {
				result += input % 10;
				input = input / 10;
			}
			if(result < 18) {
				count++;
			}
		}
		System.out.println(count);
	}
}
