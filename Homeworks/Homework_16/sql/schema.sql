create table account
(
    id          serial primary key,
    mail        char(20) unique,
    password    char(20)
);

insert into account(mail, password)
values ('marsel@gmail.com', 'qwerty007');
insert into account(mail, password)
values ('maxim@gmail.com', 'qwerty008');
insert into account(mail, password)
values ('ainur@gmail.com', 'qwerty009');
insert into account(mail, password)
values ('timur@gmail.com', 'qwerty010');