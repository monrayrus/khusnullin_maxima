package ru.maxima.repositories;

import ru.maxima.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Function;

public class UserRepositoryJDBCImpl implements UsersRepository{

    private DataSource dataSource;


    public UserRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL =
            "select id, email, password from users where email = ?";
    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from users order by id";
    //language=SQL
    private static final String SQL_UPDATE_PASSWORD =
            "update users set password = ? where id = ?;";
    //language=SQL
    private static final String SQL_INSERT =
            "insert into users(email, password) values (?, ?)";

//    private DataSource dataSource;
//
//    private final Function<ResultSet, User> rowToAccountMapper = row -> {
//        try {
//            return new User(
//                    row.getInt("id"),
//                    row.getString("email"),
//                    row.getString("password")
//            );
//        } catch (SQLException e) {
//            throw new IllegalStateException(e);
//        }
//    };

    @Override
    public void save(User user) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Can't insert");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated keys");
            }
            user.setId(generatedKeys.getInt("id"));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private final Function<ResultSet, User> rowToAccountMapper = row -> {
        try {
            return new User(
                    row.getInt("id"),
                    row.getString("email"),
                    row.getString("password")
            );
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };
    @Override
    public Optional<User> findByEmail(String email) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_EMAIL)){
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                return Optional.empty();
            }
            User user = rowToAccountMapper.apply(resultSet);
            return Optional.of(user);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(SQL_SELECT_ALL)) {

            while (result.next()) {
                User user = rowToAccountMapper.apply(result);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return users;
    }

    @Override
    public void update(User user) {
        String password = new Scanner(System.in).nextLine();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PASSWORD)) {
            statement.setString(1, password);
            statement.setInt(2, user.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
