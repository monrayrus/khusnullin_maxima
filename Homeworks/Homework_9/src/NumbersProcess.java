public interface NumbersProcess {

    int process(int number);

}
