public class Main {

    public static void main(String[] args) {

        int[] array = {12, 90, 333, 9080, 13, 635, 900, 67960};
        String[] arrayString = {"homework", "TeSt", "19digit"};
        NumbersAndStringProcessor intProcessor = new NumbersAndStringProcessor(array);
        // удаление нулей из числа
        int removedZeros[] = intProcessor.process((NumbersProcess) number -> {
            int value = number;
            int newValue = 0;

            while (value != 0) {
                int digit = value % 10;
                if (digit != 0) {
                    newValue = newValue + digit;
                    newValue = newValue * 10;
                }
                value = value / 10;
            }
            int result = newValue / 10;
            return result;
        });
        //замена нечетного числа ближайшим меньшм четным
        int[] replacedEven = intProcessor.process((NumbersProcess) number -> {
            if (number % 2 != 0) {
                return number - 1;
            }
            return number;
        });
        // разворот числа
        int[] reversedInt = intProcessor.process((NumbersProcess) number -> {
            int reverse = 0;
            while (number != 0) {
                reverse = reverse * 10;
                reverse = reverse + number % 10;
                number = number / 10;
            }
            return reverse;
        });
        NumbersAndStringProcessor stringProcessor = new NumbersAndStringProcessor(arrayString);
        // разворот строки
        String[] reversed = stringProcessor.process((StringsProcess) string -> {
                    char[] temp = string.toCharArray();
                    String result = "";
                    for (int i = temp.length - 1; i >= 0; i--) {
                        result = result + temp[i];
                    }
                    return result;
                }
        );
        //удаление числа из строки
        String[] removedDigits = stringProcessor.process((StringsProcess) string -> {
            return string.replaceAll("\\d", "");
        });
        // все маленькие буквы в большие
        String[] upperCased = stringProcessor.process((StringsProcess) string -> {
            return string.toUpperCase();
        });

        for (int i = 0; i < arrayString.length; i++) {
            System.out.print(reversed[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arrayString.length; i++) {
            System.out.print(removedDigits[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arrayString.length; i++) {
            System.out.print(upperCased[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(reversedInt[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(removedZeros[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            System.out.print(replacedEven[i] + " ");
        }
    }
}