public class NumbersAndStringProcessor {

    private String[] strings;
    private int[] array;

    public NumbersAndStringProcessor(String[] strings) {
        this.strings = strings;
    }


    public NumbersAndStringProcessor(int[] array) {
        this.array = array;
    }

    int[] process(NumbersProcess process) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = process.process(array[i]);
        }
        return result;
    }

    String[] process(StringsProcess process) {
        String[] result = new String[strings.length];
        for (int i = 0; i < strings.length; i++) {
            result[i] = process.process(strings[i]);
        }
        return result;
    }

}

