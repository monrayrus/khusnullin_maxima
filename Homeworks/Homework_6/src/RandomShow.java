import java.util.Random;

public class RandomShow {
	
	private String[][] shows;
	
	public RandomShow (String[][] shows) {
		this.shows = shows;
		}
    
    public void getShow(int channel) {
        Random random = new Random();
        int show = random.nextInt(shows.length);
        System.out.println("В данный момент на канале идет передача: " + shows[channel][show]);
    }
}
