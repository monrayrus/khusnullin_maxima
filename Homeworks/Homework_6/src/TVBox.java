public class TVBox {
	
	private ChannelList channelList;
	
	public TVBox (ChannelList channelList) {
		this.channelList = channelList;
	}

    public boolean isOn = true;

    public void channelNumber(int i) {
        if (isOn) {
            if (i <= channelList.channelArray.length && i > 0) {
                channelList.channelNumber(i);
            } else if (i == -1) {
                isOn = false;
            } else {
                System.err.println("Ошибка. Нет такого канала");
            }
        }
    }
}

