public class ChannelList {
	
	String[] channelArray;
	private String[][] shows;
	
	public ChannelList(String[] channelArray, String[][] shows) {
		this.channelArray = channelArray;
		this.shows = shows;
	} 

    public void channelNumber(int number) {
        RandomShow randomShow = new RandomShow(shows);
        for (int i = 0; i < channelArray.length; i++) {
            if (number - 1 == i) {
                System.out.println("Включен канал: " + channelArray[i]);
                randomShow.getShow(i);
            }
        }
    }
}
