import java.util.Scanner;

public class RemoteController {

	Scanner scanner = new Scanner(System.in);
	private TVBox tvbox;
	
	public RemoteController(TVBox tvbox) {
		this.tvbox = tvbox;
	}

    public void on() {
		System.out.println("Включение телевизора. Выберите канал. Для выхода выберите -1");
		while(tvbox.isOn) {
			int channelNumber = scanner.nextInt();
			tvbox.channelNumber(channelNumber);
		}
    }
}
