import java.util.Scanner;

class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        System.out.println(mainDiag(doubleArray(n, m)));

    }

    public static int[][] doubleArray(int n, int m) { //-------------Ввод матрицы
        Scanner scanner = new Scanner(System.in);
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        return array;
    }

    public static int mainDiag(int[][] array) { //------------------Метод, считающий сумму чисел главной диагонали матрицы
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j) {
                    sum += array[i][j];
                }
            }
        }
        return sum;
    }

}