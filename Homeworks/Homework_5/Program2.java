import java.util.Arrays;

class Program2 {
    public static void main(String[] args) {
        int[] array = {36, 12, 161, 312, 98, 876, 1032, 441};
        result(array, reverseArr(array));
    }

    public static int[] reverseArr(int[] array) {
        int[] reversiveArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {          //копия массива array
            reversiveArray[i] = array[i];
        }
        for (int i = 0; i < reversiveArray.length; i++) {  // прибавление к трехзначному числу его перевернутого значения
            if (reversiveArray[i] > 99 && reversiveArray[i] < 1000) {
                reversiveArray[i] = reverseNum(reversiveArray[i]) + reversiveArray[i];
            }
        }

        return reversiveArray;
    }

    public static int reverseNum(int value) {                // метод, переворачивающий число
        int result = 0;
        while (value > 0) {
            result = result * 10 + value % 10;
            value /= 10;
        }
        return result;
    }

    public static void result(int[] array1, int[] array2) {
        System.out.println(Arrays.toString(array1));        // вывод начального массива
        System.out.println(Arrays.toString(array2));        // вывод измененного массива
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) {
                System.out.println(array1[i] + " - " + array2[i]); // вывод отличающихся чисел
                break;
            }
        }
    }
}