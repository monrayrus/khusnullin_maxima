import java.util.Random;
import java.util.Scanner;

class Program4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int n = scanner.nextInt();
        System.out.println("Введите число k");
        int k = scanner.nextInt();
        oddNumbers(arrayK(arrayN(n), k));
    }

    public static int[] arrayN(int n) { // заполнение массива случайными числами
        Random random = new Random();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = random.nextInt();
        }
        return array;
    }

    public static int[] arrayK(int[] array, int k) { // ввод числа k и вставка числа от 1 до k в массив в позицию k.
        if (k < array.length && k >= 0) {
            Random random = new Random();
            array[k] = random.nextInt((k - 1) + 1);
        }
        return array;
    }

    public static void oddNumbers(int[] array) {
        System.out.println("Результат");
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                System.out.print(array[i] + " ");
            }
        }
    }

}