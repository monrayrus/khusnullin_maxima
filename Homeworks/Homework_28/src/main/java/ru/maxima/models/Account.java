package ru.maxima.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
@Builder
@Entity
public class Account implements Serializable {

    private static final long serialVersionUID = -1331728957598704392L;

    //указание роли пользователя
    public enum Role {
        ADMIN, USER
    }

    //указание статуса пользователя
    public enum State {
        CONFIRMED, NOT_CONFIRMED, BANNED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String hashPassword;

    private String name;

    private String confirmUUID;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Enumerated(value = EnumType.STRING)
    private State state;

    public boolean isAdmin() {
        return role.equals(Role.ADMIN);
    }
}
