package ru.maxima.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
@Entity
@Builder
public class Movie implements Serializable {
    private static final long serialVersionUID = 6418065998252548510L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String genre;

    private String title;

    private long year;

    @OneToMany(mappedBy = "movie")
    private List<Actor> actors;

    @OneToOne()
    private FileInfo fileInfo;
}
