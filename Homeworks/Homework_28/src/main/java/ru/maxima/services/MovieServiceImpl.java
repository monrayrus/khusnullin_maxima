package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.MovieDto;
import ru.maxima.exceptions.MovieNotFoundException;
import ru.maxima.models.Actor;
import ru.maxima.models.Movie;
import ru.maxima.repositories.ActorsRepository;
import ru.maxima.repositories.MoviesRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final ActorsRepository actorsRepository;

    private final MoviesRepository moviesRepository;

    //добавление фильма в репозиторий
    @Override
    public void addMovie(MovieDto movie) {
        Movie newMovie = Movie.builder()
                .id(movie.getId())
                .genre(movie.getGenre())
                .title(movie.getTitle())
                .year(movie.getYear())
                .build();
        moviesRepository.save(newMovie);
    }


    //получение списка всех фильмов из репозитория
    @Override
    public List<MovieDto> getAllMovies() {
        return MovieDto.from(moviesRepository.findAll());
    }

    //добавление актера к фильму
    @Override
    public void addActorToMovie(Long movieId, ActorDto actorForm) {
        Movie movie = moviesRepository.getById(movieId);
        Actor actor = actorsRepository.getById(actorForm.getId());
        actor.setMovie(movie);
        actorsRepository.save(actor);
    }

    //получение актеров по фильму
    @Override
    public List<ActorDto> getActorsByMovie(Long movieId) {
        moviesRepository.findById(movieId).orElseThrow(MovieNotFoundException::new);
        return ActorDto.from(actorsRepository.findAllByMovieId(movieId));
    }



    //получение незанятых актеров
    @Override
    public List<ActorDto> getActorsWithoutMovie() {
        return ActorDto.from(actorsRepository.findAllByMovieIsNull());
    }
}
