package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final AccountsRepository accountsRepository;

    @Override
    public AccountDto getUser(Long currentUserId) {
        return AccountDto.from(accountsRepository.getById(currentUserId));
    }

    @Override
    public void updateUser(Long currentUserId, String name) {
        Account account = accountsRepository.getById(currentUserId);
        account.setName(name);
    }
}
