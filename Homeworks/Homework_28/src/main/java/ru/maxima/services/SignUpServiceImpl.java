package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.maxima.dto.SignUpForm;
import ru.maxima.mail.MailUtil;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final AccountsRepository accountsRepository;

    private final PasswordEncoder passwordEncoder;

    private final MailUtil mailUtil;

    //регистрация пользователя и отправка письма на почту
    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .name(form.getName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.NOT_CONFIRMED)
                .confirmUUID(UUID.randomUUID().toString())
                .build();
        mailUtil.sendMailToConfirm(account.getName(), account.getEmail(), account.getConfirmUUID());
        accountsRepository.save(account);
    }
}
