package ru.maxima.services;

import ru.maxima.dto.AccountDto;
import ru.maxima.models.Account;

public interface ProfileService {
    AccountDto getUser(Long currentUserId);

    void updateUser(Long currentUserId, String name);
}
