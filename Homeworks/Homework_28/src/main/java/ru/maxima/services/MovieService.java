package ru.maxima.services;

import ru.maxima.dto.ActorDto;
import ru.maxima.dto.MovieDto;

import java.util.List;

public interface MovieService {

    void addMovie(MovieDto movie);

    List<MovieDto> getAllMovies();

    void addActorToMovie(Long movieId, ActorDto actor);

    List<ActorDto> getActorsByMovie(Long movieId);

    List<ActorDto> getActorsWithoutMovie();
}
