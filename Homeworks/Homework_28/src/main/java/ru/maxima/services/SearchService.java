package ru.maxima.services;

import ru.maxima.dto.MovieDto;
import java.util.List;

public interface SearchService {
    List<MovieDto> searchMovieByTitle(String title);
}
