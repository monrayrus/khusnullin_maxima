package ru.maxima.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface FilesService {
    void saveFile(MultipartFile file, String description, Long movieId);

    void addFileToResponse(Long movieId, HttpServletResponse response);

    //String getFileNameById(Long movieId);
}
