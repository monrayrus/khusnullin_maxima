package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.ActorDto;
import ru.maxima.models.Actor;
import ru.maxima.repositories.ActorsRepository;

import java.util.List;

import static ru.maxima.dto.ActorDto.from;

@Service
@RequiredArgsConstructor
public class ActorServiceImpl implements ActorService {

    private final ActorsRepository actorsRepository;

    //Создание актера через билдер
    @Override
    public void addActor(ActorDto actorDto) {
        Actor actor = Actor.builder()
                .id(actorDto.getId())
                .firstName(actorDto.getFirstName())
                .lastName(actorDto.getLastName())
                .build();
        actorsRepository.save(actor);
    }

    //Получение списка всех актеров
    @Override
    public List<ActorDto> getAllActors() {
        return from(actorsRepository.findAll());
    }
}
