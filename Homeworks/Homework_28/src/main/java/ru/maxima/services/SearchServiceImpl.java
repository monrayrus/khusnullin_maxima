package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.MovieDto;
import ru.maxima.repositories.MoviesRepository;

import java.util.List;

import static ru.maxima.dto.MovieDto.from;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final MoviesRepository moviesRepository;

    @Override
    public List<MovieDto> searchMovieByTitle(String title) {
        return from(moviesRepository.searchMovieByTitleLike("%" + title + "%"));
    }
}
