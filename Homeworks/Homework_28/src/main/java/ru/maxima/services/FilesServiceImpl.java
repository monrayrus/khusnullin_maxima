package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.maxima.models.FileInfo;
import ru.maxima.models.Movie;
import ru.maxima.repositories.FilesInfoRepository;
import ru.maxima.repositories.MoviesRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class FilesServiceImpl implements FilesService {

    private final FilesInfoRepository filesInfoRepository;
    private final MoviesRepository moviesRepository;

    @Value("C:\\Users\\79638\\Desktop\\khusnullin_maxima\\Homeworks\\Homework_28\\storage")
    //@Value("C:\\Users\\monra\\Desktop\\khusnullin_maxima\\Homeworks\\Homework_28\\storage")
    private String storageFolder;

    @Override
    public void saveFile(MultipartFile file, String description, Long movieId) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        FileInfo fileInfo = FileInfo.builder()
                .description(description)
                .movie(moviesRepository.getById(movieId))
                .mimeType(file.getContentType())
                .originalFileName(file.getOriginalFilename())
                .storageFileName(UUID.randomUUID() + "." + extension)
                .size(file.getSize())
                .build();

        filesInfoRepository.save(fileInfo);
        Movie movie = moviesRepository.getById(movieId);
        movie.setFileInfo(fileInfo);

        try {
            Files.copy(file.getInputStream(), Paths.get(storageFolder, fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }

    @Override
    public void addFileToResponse(Long movieId, HttpServletResponse response) {
        FileInfo fileInfo = filesInfoRepository.findByMovieId(movieId);
        String fileName = fileInfo.getStorageFileName();
        response.setContentType(fileInfo.getMimeType());
        response.setContentLength(fileInfo.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + fileInfo.getOriginalFileName() + "\"");
        try {
            IOUtils.copy(new FileInputStream(storageFolder + "/" + fileName), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

}
