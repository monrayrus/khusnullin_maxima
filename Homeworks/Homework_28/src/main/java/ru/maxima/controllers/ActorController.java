package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.maxima.dto.ActorDto;
import ru.maxima.services.ActorService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ActorController {
    @Autowired
    private ActorService actorService;

    @RequestMapping("/actors")
    public String getActorsPage() {
        return "actors";
    }

    @RequestMapping(value = "/actors", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public List<ActorDto> addActor(@RequestBody ActorDto actor) {
        actorService.addActor(actor);
        return actorService.getAllActors();
    }

}
