package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.maxima.services.FilesService;

import javax.servlet.http.HttpServletResponse;


@RequiredArgsConstructor
@Controller
public class FilesController {

    private final FilesService filesService;

    @GetMapping("/films/{filmId}/upload")
    public String getFilesUploadPage(@PathVariable("filmId") Long filmId, Model model) {
        model.addAttribute("filmId", filmId);
        return "file_upload_page";
    }

    @PostMapping("/films/{filmId}/upload")
    public String uploadFile(@RequestParam("file")  MultipartFile file, @RequestParam("description") String description, @PathVariable("filmId") Long filmId) {
        filesService.saveFile(file, description, filmId);
        return "file_upload_page";
    }

    @GetMapping("/films/{filmId}/file")
    public void getFile(HttpServletResponse response, @PathVariable("filmId") Long movieId) {
        filesService.addFileToResponse(movieId, response);
    }
}
