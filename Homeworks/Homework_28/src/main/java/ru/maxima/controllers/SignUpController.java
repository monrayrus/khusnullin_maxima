package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maxima.dto.SignUpForm;
import ru.maxima.services.SignUpService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class SignUpController {
    private final SignUpService signUpService;

    //возвращает страницу с содержимым
    @RequestMapping("/signUp")
    public String getSignUpPage(Authentication authentication, Model model) {
        if (authentication != null) {
            return "redirect:/";
        }
        model.addAttribute("signUpForm", new SignUpForm());
        return "sign_up";
    }

    //отправляет данные для регистрации, при успешной регистрации отправляет на страницу авторизации
    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public String signUp(@Valid SignUpForm form, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("signUpForm", form);
            return "sign_up";
        }
        signUpService.signUp(form);
        return "redirect:/signIn";
    }
}
