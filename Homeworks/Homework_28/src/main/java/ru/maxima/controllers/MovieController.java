package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.maxima.dto.ActorDto;
import ru.maxima.dto.MovieDto;
import ru.maxima.services.MovieService;

@Controller
@RequiredArgsConstructor
public class MovieController {
    @Autowired
    private MovieService movieService;

    @RequestMapping("/films")
    public String getMovies(@AuthenticationPrincipal(expression = "account.isAdmin()") Boolean isAdmin, Model model) {
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("films", movieService.getAllMovies());
        return "films";
    }

    @RequestMapping("/films/{filmId}/actors")
    public String getActorsOfMoviePage(@AuthenticationPrincipal(expression = "account.isAdmin()") Boolean isAdmin, @PathVariable("filmId") Long filmId, Model model) {
        model.addAttribute("actors", movieService.getActorsByMovie(filmId));
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("actors_free", movieService.getActorsWithoutMovie());
        return "actors_in_film";
    }

    @RequestMapping(value = "/films/{filmId}/actors", method = RequestMethod.POST)
    public String addActorToMovie(@PathVariable("filmId") Long filmId, ActorDto actor) {
        movieService.addActorToMovie(filmId, actor);
        return "redirect:/films/" + filmId + "/actors";
    }


    @RequestMapping(value = "/films", method = RequestMethod.POST)
    public String addMovie(MovieDto form) {
        movieService.addMovie(form);
        return "redirect:/films";
    }
}
