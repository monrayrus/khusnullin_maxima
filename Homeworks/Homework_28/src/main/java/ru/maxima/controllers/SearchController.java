package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.maxima.dto.AccountDto;
import ru.maxima.dto.MovieDto;
import ru.maxima.services.SearchService;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/search")
public class SearchController {

    private final SearchService searchService;

    @GetMapping
    public String getSearchPage() {
        return "search";
    }

    @GetMapping(value = "/films")
    @ResponseBody
    public List<MovieDto> searchMovie(@RequestParam("title") String title) {
        return searchService.searchMovieByTitle(title);
    }
}
