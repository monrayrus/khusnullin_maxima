package ru.maxima.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class ErrorsController {
    @GetMapping("/404")
    public String get404page() {
        return "errors/error404";
    }

    @PostMapping("/500/anfe")
    public String get500ForActorNotFoundException() {
            return "errors/error500anfe";
        }
    }
