package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.maxima.services.ProfileService;

@RequiredArgsConstructor
@Controller
public class ProfileController {

    private final ProfileService profileService;

    //returns active user profile page
    @GetMapping("/profile")
    public String getProfilePage(@AuthenticationPrincipal(expression = "id") Long currentUserId, Model model) {
        model.addAttribute("user", profileService.getUser(currentUserId));
        return "profile";
    }

    //should be update active user name and return profile page, but returns freemarker error
    @PostMapping("/profile")
    public String updateUserName(@AuthenticationPrincipal(expression = "id") Long currentUserId,
                                 @RequestParam("newName") String name, Model model) {
        model.addAttribute("newName", name);
        profileService.updateUser(currentUserId, name);
        model.addAttribute("user", profileService.getUser(currentUserId));
        return "profile";

    }
}
