package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.models.Account;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto implements Serializable {

    private static final long serialVersionUID = -8227317333627157801L;
    private Long id;
    private String email;
    private String name;

    public static AccountDto from(Account account) {
        return AccountDto.builder()
                .email(account.getEmail())
                .id(account.getId())
                .name(account.getName())
                .build();
    }

    public static List<AccountDto> from(List<Account> accounts) {
        return accounts.stream().map(AccountDto::from).collect(Collectors.toList());
    }
}
