package ru.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.maxima.models.Movie;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MovieDto {
    private long id;
    private String title;
    private String genre;
    private long year;

    public static MovieDto from(Movie movie) {
        return MovieDto.builder()
                .id(movie.getId())
                .title(movie.getTitle())
                .genre(movie.getGenre())
                .year(movie.getYear())
                .build();
    }

    public static List<MovieDto> from(List<Movie> movies) {
        return movies.stream().map(MovieDto::from).collect(Collectors.toList());
    }
}
