package ru.maxima.mail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Component
public class MailUtil {
    @Autowired
    private JavaMailSender mailSender;

    @Value("${mail.util.serverUrl}")
    private String serverUrl;

    @Value("${mail.util.signUpSubject}")
    private String signUpSubject;

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private Configuration forEmailConfig;

    public void sendMailToConfirm(String name, String to, String uuid){
        try{
            Template template = forEmailConfig.getTemplate("confirm_email.ftlh");
            StringWriter html = processForEmailConfirm(name, uuid, template);
            sendMail(to, signUpSubject, html);

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public StringWriter processForEmailConfirm(String name, String uuid, Template template) throws TemplateException, IOException {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("name", name);
        attributes.put("confirmLink", serverUrl + "/confirm/" + uuid);
        StringWriter html = new StringWriter();
        template.process(attributes, html);
        return html;
    }


    //упаковывает письмо для отправки с определенными параметрами с помощью preparator
    public void sendMail(String to, String subject, StringWriter html) {
        final String mailText = html.toString();
        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setSubject(subject);
            helper.setTo(to);
            helper.setFrom(from);
            helper.setText(mailText, true);
        };
        mailSender.send(preparator);
    }
}
