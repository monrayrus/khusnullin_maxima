package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Movie;

import java.util.List;
import java.util.Optional;

public interface MoviesRepository extends JpaRepository<Movie, Long> {

    List<Movie> findAll();

    List<Movie> searchMovieByTitleLike(String title);
}
