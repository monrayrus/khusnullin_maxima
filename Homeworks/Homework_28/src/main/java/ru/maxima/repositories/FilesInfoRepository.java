package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.FileInfo;

public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findByMovieId(Long movieId);
}
