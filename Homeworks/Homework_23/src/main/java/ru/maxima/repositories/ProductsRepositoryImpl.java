package ru.maxima.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.maxima.models.Product;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

@Repository
public class ProductsRepositoryImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into products(product_name) values (:product) RETURNING id";

    //language=SQL
    private static final String SQL_SELECT_ALL =
            "select * from products order by id";

    //language=SQL
    private static final String SQL_FIND_MILK =
            "select * from products where product_name like '%()milk' order by id";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .product_name(row.getString("product_name"))
            .build();

    @Autowired
    public ProductsRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, (new MapSqlParameterSource()
                .addValue("product", product.getProduct_name())), keyHolder, new String[]{"id"});
                product.setId(keyHolder.getKey().longValue());
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findMilk() {
        return namedParameterJdbcTemplate.query(SQL_FIND_MILK,
                productRowMapper);
    }


}
