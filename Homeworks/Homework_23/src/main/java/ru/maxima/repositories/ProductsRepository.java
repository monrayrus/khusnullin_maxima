package ru.maxima.repositories;

import ru.maxima.models.Product;

import java.util.List;

public interface ProductsRepository {
    void save (Product product);
    List<Product> findAll();
    List<Product> findMilk();
}
