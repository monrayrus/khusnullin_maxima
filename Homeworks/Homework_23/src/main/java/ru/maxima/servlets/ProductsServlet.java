package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.config.ApplicationConfig;
import ru.maxima.services.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/allProducts")
public class ProductsServlet extends HttpServlet {

        private ProductService productService;

        @Override
        public void init() throws ServletException {
            ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
            this.productService = context.getBean(ProductService.class);
        }

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            PrintWriter writer = response.getWriter();

            writer.println("<h1>Products</h1>");
            writer.println("<table>");
            writer.println("    <tr>");
            writer.println("        <th>" + "Name of product" + "</th>");
            writer.println("    </tr>");
            for (String product : productService.getAllProducts()) {
                writer.println("<tr>");
                writer.println("    <td>" + product + "</td>");
                writer.println("</tr>");
            }
            writer.println("</table>");
        }
    }

