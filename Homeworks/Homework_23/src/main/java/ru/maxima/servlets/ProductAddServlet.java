package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.config.ApplicationConfig;
import ru.maxima.services.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addProducts")
public class ProductAddServlet extends HttpServlet {

    private ProductService productService;

        @Override
        public void init() throws ServletException {
            ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
            this.productService = context.getBean(ProductService.class);
        }

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.getWriter().println("<h1>Add new product</h1>\n" +
                    "<p>Please enter product name</p>\n" +
                    "<br>\n" +
                    "<form method=\"post\">\n" +
                    "\t<label for=\"product_name\">Enter product here:</label>\n" +
                    "\t<input id=\"product_name\" type=\"text\" name=\"product_name\" placeholder=\"Your product\">\n" +
                    "\t<input type=\"submit\" name=\"\" value=\"Add\">\n" +
                    "</form>");
        }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String product_name = request.getParameter("product_name");
        productService.addProduct(product_name);
        response.sendRedirect("/Homework_23_0_1_war/allProducts");
    }
}
