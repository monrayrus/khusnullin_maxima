package ru.maxima.services;

import java.util.List;

public interface ProductService {
    void addProduct(String product);
    List<String> getAllProducts();
    List<String> getAllMilk();
}
