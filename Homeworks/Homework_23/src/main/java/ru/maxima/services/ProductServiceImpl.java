package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.models.Product;
import ru.maxima.repositories.ProductsRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProduct(String product_name) {
        Product product = Product.builder()
        .product_name(product_name)
        .build();

        productsRepository.save(product);
    }

    @Override
    public List<String> getAllProducts() {
        return productsRepository.findAll().stream().map(Product::getProduct_name).collect(Collectors.toList());
    }

    @Override
    public List<String> getAllMilk() {
        return productsRepository.findMilk().stream().map(Product::getProduct_name).collect(Collectors.toList());
    }
}
